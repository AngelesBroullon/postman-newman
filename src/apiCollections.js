const newman = require('newman')

const testFolder = './src/collections/';
const fs = require('fs');

fs.readdir(testFolder, (err, files) => {
    files.forEach(file => {
        newman.run({
            collection: require(`./collections/${file}`),
            reporters: 'cli',
        })
    });
});

