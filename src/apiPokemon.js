const newman = require('newman')

newman.run({
    collection: require('./collections/PokeAPI.postman_collection.json'),
    reporters: 'cli',
})
