# API Testing using postman collections

A basic template for performing API tests, using the postman collections.

This project contemplates 2 main cases:
* testing a single collection, (see `apiPokemonTest.js`).
* testing all the collections on a directory (see `apiCollectionsTest.js`).
* testing all the collections on a directory (see `apiCollectionsTest-reportHTML.js`), and getting the report on HTML.


## Setup

* Run the command `npm install`
* Install istambul for code coverage reports with `npm install istanbul`


## Scripts

* `npm run pokemon`: this is an example to test a single collection.
* `npm run e2e`: this example tests all the collections inside a folder.
* `npm run report-newman`: this example tests all the collections inside a folder, and get an HTML on the `newman` directory.
* `npm test`: launch jasmine tests, for basic report.
* `npm coverage`: launch jasmine tests, generating code coverture.

### Comments
* you may get a full coverage report on HTML by manually calling on `nyc --reporter=lcov --reporter=text-summary npm run test`
